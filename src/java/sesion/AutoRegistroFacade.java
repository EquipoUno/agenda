/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sesion;

import entidad.AutoRegistro;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Daftzero
 */
@Stateless
public class AutoRegistroFacade extends AbstractFacade<AutoRegistro> {
    @PersistenceContext(unitName = "agendatest3PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AutoRegistroFacade() {
        super(AutoRegistro.class);
    }
    
}
