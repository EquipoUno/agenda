/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reunion.findAll", query = "SELECT r FROM Reunion r"),
    @NamedQuery(name = "Reunion.findByIdreunion", query = "SELECT r FROM Reunion r WHERE r.idreunion = :idreunion")})
public class Reunion implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idreunion")
    private Integer idreunion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reunionIdreunion", fetch = FetchType.EAGER)
    private Collection<ReunionProveedores> reunionProveedoresCollection;
    @JoinColumn(name = "agenda_idAgenda", referencedColumnName = "idAgenda")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Agenda agendaidAgenda;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reunionIdreunion", fetch = FetchType.EAGER)
    private Collection<ReunionEmpleados> reunionEmpleadosCollection;

    public Reunion() {
    }

    public Reunion(Integer idreunion) {
        this.idreunion = idreunion;
    }

    public Integer getIdreunion() {
        return idreunion;
    }

    public void setIdreunion(Integer idreunion) {
        this.idreunion = idreunion;
    }

    @XmlTransient
    public Collection<ReunionProveedores> getReunionProveedoresCollection() {
        return reunionProveedoresCollection;
    }

    public void setReunionProveedoresCollection(Collection<ReunionProveedores> reunionProveedoresCollection) {
        this.reunionProveedoresCollection = reunionProveedoresCollection;
    }

    public Agenda getAgendaidAgenda() {
        return agendaidAgenda;
    }

    public void setAgendaidAgenda(Agenda agendaidAgenda) {
        this.agendaidAgenda = agendaidAgenda;
    }

    @XmlTransient
    public Collection<ReunionEmpleados> getReunionEmpleadosCollection() {
        return reunionEmpleadosCollection;
    }

    public void setReunionEmpleadosCollection(Collection<ReunionEmpleados> reunionEmpleadosCollection) {
        this.reunionEmpleadosCollection = reunionEmpleadosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idreunion != null ? idreunion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reunion)) {
            return false;
        }
        Reunion other = (Reunion) object;
        if ((this.idreunion == null && other.idreunion != null) || (this.idreunion != null && !this.idreunion.equals(other.idreunion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Reunion[ idreunion=" + idreunion + " ]";
    }
    
}
