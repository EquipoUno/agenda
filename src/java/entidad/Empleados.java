/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "empleados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleados.findAll", query = "SELECT e FROM Empleados e"),
    @NamedQuery(name = "Empleados.findByIdEmpleados", query = "SELECT e FROM Empleados e WHERE e.idEmpleados = :idEmpleados"),
    @NamedQuery(name = "Empleados.findByNombre", query = "SELECT e FROM Empleados e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Empleados.findByApellido", query = "SELECT e FROM Empleados e WHERE e.apellido = :apellido"),
    @NamedQuery(name = "Empleados.findByEmailP", query = "SELECT e FROM Empleados e WHERE e.emailP = :emailP"),
    @NamedQuery(name = "Empleados.findByEmailE", query = "SELECT e FROM Empleados e WHERE e.emailE = :emailE"),
    @NamedQuery(name = "Empleados.findByTelefono", query = "SELECT e FROM Empleados e WHERE e.telefono = :telefono"),
    @NamedQuery(name = "Empleados.findByNss", query = "SELECT e FROM Empleados e WHERE e.nss = :nss")})
public class Empleados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idEmpleados")
    private Integer idEmpleados;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "emailP")
    private String emailP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "emailE")
    private String emailE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nss")
    private String nss;
    @JoinColumn(name = "idUsers", referencedColumnName = "idUsuarios")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuarios idUsers;
    @OneToMany(mappedBy = "empleadosidEmpleados", fetch = FetchType.EAGER)
    private Collection<EventoParticipantes> eventoParticipantesCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "empleadosidEmpleados", fetch = FetchType.EAGER)
    private Collection<ReunionEmpleados> reunionEmpleadosCollection;

    public Empleados() {
    }

    public Empleados(Integer idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    public Empleados(Integer idEmpleados, String nombre, String apellido, String emailP, String emailE, String telefono, String nss) {
        this.idEmpleados = idEmpleados;
        this.nombre = nombre;
        this.apellido = apellido;
        this.emailP = emailP;
        this.emailE = emailE;
        this.telefono = telefono;
        this.nss = nss;
    }

    public Integer getIdEmpleados() {
        return idEmpleados;
    }

    public void setIdEmpleados(Integer idEmpleados) {
        this.idEmpleados = idEmpleados;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmailP() {
        return emailP;
    }

    public void setEmailP(String emailP) {
        this.emailP = emailP;
    }

    public String getEmailE() {
        return emailE;
    }

    public void setEmailE(String emailE) {
        this.emailE = emailE;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public Usuarios getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Usuarios idUsers) {
        this.idUsers = idUsers;
    }

    @XmlTransient
    public Collection<EventoParticipantes> getEventoParticipantesCollection() {
        return eventoParticipantesCollection;
    }

    public void setEventoParticipantesCollection(Collection<EventoParticipantes> eventoParticipantesCollection) {
        this.eventoParticipantesCollection = eventoParticipantesCollection;
    }

    @XmlTransient
    public Collection<ReunionEmpleados> getReunionEmpleadosCollection() {
        return reunionEmpleadosCollection;
    }

    public void setReunionEmpleadosCollection(Collection<ReunionEmpleados> reunionEmpleadosCollection) {
        this.reunionEmpleadosCollection = reunionEmpleadosCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEmpleados != null ? idEmpleados.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleados)) {
            return false;
        }
        Empleados other = (Empleados) object;
        if ((this.idEmpleados == null && other.idEmpleados != null) || (this.idEmpleados != null && !this.idEmpleados.equals(other.idEmpleados))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Empleados[ idEmpleados=" + idEmpleados + " ]";
    }
    
}
