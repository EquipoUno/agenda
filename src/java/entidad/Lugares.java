/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "lugares")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lugares.findAll", query = "SELECT l FROM Lugares l"),
    @NamedQuery(name = "Lugares.findByIdLugares", query = "SELECT l FROM Lugares l WHERE l.idLugares = :idLugares"),
    @NamedQuery(name = "Lugares.findByNombre", query = "SELECT l FROM Lugares l WHERE l.nombre = :nombre"),
    @NamedQuery(name = "Lugares.findByCapcidad", query = "SELECT l FROM Lugares l WHERE l.capcidad = :capcidad")})
public class Lugares implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idLugares")
    private Integer idLugares;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "capcidad")
    private Integer capcidad;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idLugares", fetch = FetchType.EAGER)
    private Collection<Agenda> agendaCollection;

    public Lugares() {
    }

    public Lugares(Integer idLugares) {
        this.idLugares = idLugares;
    }

    public Integer getIdLugares() {
        return idLugares;
    }

    public void setIdLugares(Integer idLugares) {
        this.idLugares = idLugares;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCapcidad() {
        return capcidad;
    }

    public void setCapcidad(Integer capcidad) {
        this.capcidad = capcidad;
    }

    @XmlTransient
    public Collection<Agenda> getAgendaCollection() {
        return agendaCollection;
    }

    public void setAgendaCollection(Collection<Agenda> agendaCollection) {
        this.agendaCollection = agendaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idLugares != null ? idLugares.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lugares)) {
            return false;
        }
        Lugares other = (Lugares) object;
        if ((this.idLugares == null && other.idLugares != null) || (this.idLugares != null && !this.idLugares.equals(other.idLugares))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Lugares[ idLugares=" + idLugares + " ]";
    }
    
}
