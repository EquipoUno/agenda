/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "auto_registro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AutoRegistro.findAll", query = "SELECT a FROM AutoRegistro a"),
    @NamedQuery(name = "AutoRegistro.findByIdAutoRegistro", query = "SELECT a FROM AutoRegistro a WHERE a.idAutoRegistro = :idAutoRegistro"),
    @NamedQuery(name = "AutoRegistro.findByNombre", query = "SELECT a FROM AutoRegistro a WHERE a.nombre = :nombre"),
    @NamedQuery(name = "AutoRegistro.findByApellido", query = "SELECT a FROM AutoRegistro a WHERE a.apellido = :apellido"),
    @NamedQuery(name = "AutoRegistro.findByEmailP", query = "SELECT a FROM AutoRegistro a WHERE a.emailP = :emailP"),
    @NamedQuery(name = "AutoRegistro.findByEmailE", query = "SELECT a FROM AutoRegistro a WHERE a.emailE = :emailE"),
    @NamedQuery(name = "AutoRegistro.findByTelefonoP", query = "SELECT a FROM AutoRegistro a WHERE a.telefonoP = :telefonoP"),
    @NamedQuery(name = "AutoRegistro.findByNss", query = "SELECT a FROM AutoRegistro a WHERE a.nss = :nss"),
    @NamedQuery(name = "AutoRegistro.findByEmpresa", query = "SELECT a FROM AutoRegistro a WHERE a.empresa = :empresa"),
    @NamedQuery(name = "AutoRegistro.findByTelefonoE", query = "SELECT a FROM AutoRegistro a WHERE a.telefonoE = :telefonoE")})
public class AutoRegistro implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idAutoRegistro")
    private Integer idAutoRegistro;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "emailP")
    private String emailP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "emailE")
    private String emailE;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "telefonoP")
    private String telefonoP;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nss")
    private String nss;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "empresa")
    private String empresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "telefonoE")
    private String telefonoE;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idAutoRegistro", fetch = FetchType.EAGER)
    private Collection<EventoParticipantes> eventoParticipantesCollection;

    public AutoRegistro() {
    }

    public AutoRegistro(Integer idAutoRegistro) {
        this.idAutoRegistro = idAutoRegistro;
    }

    public AutoRegistro(Integer idAutoRegistro, String nombre, String apellido, String emailP, String emailE, String telefonoP, String nss, String empresa, String telefonoE) {
        this.idAutoRegistro = idAutoRegistro;
        this.nombre = nombre;
        this.apellido = apellido;
        this.emailP = emailP;
        this.emailE = emailE;
        this.telefonoP = telefonoP;
        this.nss = nss;
        this.empresa = empresa;
        this.telefonoE = telefonoE;
    }

    public Integer getIdAutoRegistro() {
        return idAutoRegistro;
    }

    public void setIdAutoRegistro(Integer idAutoRegistro) {
        this.idAutoRegistro = idAutoRegistro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmailP() {
        return emailP;
    }

    public void setEmailP(String emailP) {
        this.emailP = emailP;
    }

    public String getEmailE() {
        return emailE;
    }

    public void setEmailE(String emailE) {
        this.emailE = emailE;
    }

    public String getTelefonoP() {
        return telefonoP;
    }

    public void setTelefonoP(String telefonoP) {
        this.telefonoP = telefonoP;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getTelefonoE() {
        return telefonoE;
    }

    public void setTelefonoE(String telefonoE) {
        this.telefonoE = telefonoE;
    }

    @XmlTransient
    public Collection<EventoParticipantes> getEventoParticipantesCollection() {
        return eventoParticipantesCollection;
    }

    public void setEventoParticipantesCollection(Collection<EventoParticipantes> eventoParticipantesCollection) {
        this.eventoParticipantesCollection = eventoParticipantesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAutoRegistro != null ? idAutoRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AutoRegistro)) {
            return false;
        }
        AutoRegistro other = (AutoRegistro) object;
        if ((this.idAutoRegistro == null && other.idAutoRegistro != null) || (this.idAutoRegistro != null && !this.idAutoRegistro.equals(other.idAutoRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.AutoRegistro[ idAutoRegistro=" + idAutoRegistro + " ]";
    }
    
}
