/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunion-proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReunionProveedores.findAll", query = "SELECT r FROM ReunionProveedores r"),
    @NamedQuery(name = "ReunionProveedores.findByIdRP", query = "SELECT r FROM ReunionProveedores r WHERE r.idRP = :idRP")})
public class ReunionProveedores implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRP")
    private Integer idRP;
    @JoinColumn(name = "idProveedores", referencedColumnName = "idProveedores")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Proveedores idProveedores;
    @JoinColumn(name = "reunion_idreunion", referencedColumnName = "idreunion")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Reunion reunionIdreunion;

    public ReunionProveedores() {
    }

    public ReunionProveedores(Integer idRP) {
        this.idRP = idRP;
    }

    public Integer getIdRP() {
        return idRP;
    }

    public void setIdRP(Integer idRP) {
        this.idRP = idRP;
    }

    public Proveedores getIdProveedores() {
        return idProveedores;
    }

    public void setIdProveedores(Proveedores idProveedores) {
        this.idProveedores = idProveedores;
    }

    public Reunion getReunionIdreunion() {
        return reunionIdreunion;
    }

    public void setReunionIdreunion(Reunion reunionIdreunion) {
        this.reunionIdreunion = reunionIdreunion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRP != null ? idRP.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReunionProveedores)) {
            return false;
        }
        ReunionProveedores other = (ReunionProveedores) object;
        if ((this.idRP == null && other.idRP != null) || (this.idRP != null && !this.idRP.equals(other.idRP))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.ReunionProveedores[ idRP=" + idRP + " ]";
    }
    
}
