/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "reunion-empleados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReunionEmpleados.findAll", query = "SELECT r FROM ReunionEmpleados r"),
    @NamedQuery(name = "ReunionEmpleados.findByIdRE", query = "SELECT r FROM ReunionEmpleados r WHERE r.idRE = :idRE")})
public class ReunionEmpleados implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idRE")
    private Integer idRE;
    @JoinColumn(name = "empleados_idEmpleados", referencedColumnName = "idEmpleados")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Empleados empleadosidEmpleados;
    @JoinColumn(name = "reunion_idreunion", referencedColumnName = "idreunion")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Reunion reunionIdreunion;

    public ReunionEmpleados() {
    }

    public ReunionEmpleados(Integer idRE) {
        this.idRE = idRE;
    }

    public Integer getIdRE() {
        return idRE;
    }

    public void setIdRE(Integer idRE) {
        this.idRE = idRE;
    }

    public Empleados getEmpleadosidEmpleados() {
        return empleadosidEmpleados;
    }

    public void setEmpleadosidEmpleados(Empleados empleadosidEmpleados) {
        this.empleadosidEmpleados = empleadosidEmpleados;
    }

    public Reunion getReunionIdreunion() {
        return reunionIdreunion;
    }

    public void setReunionIdreunion(Reunion reunionIdreunion) {
        this.reunionIdreunion = reunionIdreunion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRE != null ? idRE.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReunionEmpleados)) {
            return false;
        }
        ReunionEmpleados other = (ReunionEmpleados) object;
        if ((this.idRE == null && other.idRE != null) || (this.idRE != null && !this.idRE.equals(other.idRE))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.ReunionEmpleados[ idRE=" + idRE + " ]";
    }
    
}
