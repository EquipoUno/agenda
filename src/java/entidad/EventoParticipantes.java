/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "evento-participantes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventoParticipantes.findAll", query = "SELECT e FROM EventoParticipantes e"),
    @NamedQuery(name = "EventoParticipantes.findByIdParticipante", query = "SELECT e FROM EventoParticipantes e WHERE e.idParticipante = :idParticipante")})
public class EventoParticipantes implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "idParticipante")
    private Integer idParticipante;
    @JoinColumn(name = "idAutoRegistro", referencedColumnName = "idAutoRegistro")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private AutoRegistro idAutoRegistro;
    @JoinColumn(name = "empleados_idEmpleados", referencedColumnName = "idEmpleados")
    @ManyToOne(fetch = FetchType.EAGER)
    private Empleados empleadosidEmpleados;
    @JoinColumn(name = "evento_idevento", referencedColumnName = "idevento")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Evento eventoIdevento;
    @JoinColumn(name = "proveedores_idProveedores", referencedColumnName = "idProveedores")
    @ManyToOne(fetch = FetchType.EAGER)
    private Proveedores proveedoresidProveedores;

    public EventoParticipantes() {
    }

    public EventoParticipantes(Integer idParticipante) {
        this.idParticipante = idParticipante;
    }

    public Integer getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(Integer idParticipante) {
        this.idParticipante = idParticipante;
    }

    public AutoRegistro getIdAutoRegistro() {
        return idAutoRegistro;
    }

    public void setIdAutoRegistro(AutoRegistro idAutoRegistro) {
        this.idAutoRegistro = idAutoRegistro;
    }

    public Empleados getEmpleadosidEmpleados() {
        return empleadosidEmpleados;
    }

    public void setEmpleadosidEmpleados(Empleados empleadosidEmpleados) {
        this.empleadosidEmpleados = empleadosidEmpleados;
    }

    public Evento getEventoIdevento() {
        return eventoIdevento;
    }

    public void setEventoIdevento(Evento eventoIdevento) {
        this.eventoIdevento = eventoIdevento;
    }

    public Proveedores getProveedoresidProveedores() {
        return proveedoresidProveedores;
    }

    public void setProveedoresidProveedores(Proveedores proveedoresidProveedores) {
        this.proveedoresidProveedores = proveedoresidProveedores;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idParticipante != null ? idParticipante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventoParticipantes)) {
            return false;
        }
        EventoParticipantes other = (EventoParticipantes) object;
        if ((this.idParticipante == null && other.idParticipante != null) || (this.idParticipante != null && !this.idParticipante.equals(other.idParticipante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.EventoParticipantes[ idParticipante=" + idParticipante + " ]";
    }
    
}
