/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entidad;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Daftzero
 */
@Entity
@Table(name = "proveedores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proveedores.findAll", query = "SELECT p FROM Proveedores p"),
    @NamedQuery(name = "Proveedores.findByIdProveedores", query = "SELECT p FROM Proveedores p WHERE p.idProveedores = :idProveedores"),
    @NamedQuery(name = "Proveedores.findByNombre", query = "SELECT p FROM Proveedores p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Proveedores.findByNombreContacto", query = "SELECT p FROM Proveedores p WHERE p.nombreContacto = :nombreContacto"),
    @NamedQuery(name = "Proveedores.findByTelefonoContacto", query = "SELECT p FROM Proveedores p WHERE p.telefonoContacto = :telefonoContacto"),
    @NamedQuery(name = "Proveedores.findByTelefonoEmpresa", query = "SELECT p FROM Proveedores p WHERE p.telefonoEmpresa = :telefonoEmpresa"),
    @NamedQuery(name = "Proveedores.findByEmailContacto", query = "SELECT p FROM Proveedores p WHERE p.emailContacto = :emailContacto"),
    @NamedQuery(name = "Proveedores.findByDomicilio", query = "SELECT p FROM Proveedores p WHERE p.domicilio = :domicilio")})
public class Proveedores implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idProveedores")
    private Integer idProveedores;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "nombre_contacto")
    private String nombreContacto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "telefono_contacto")
    private String telefonoContacto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "telefono_empresa")
    private String telefonoEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "email_contacto")
    private String emailContacto;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "domicilio")
    private String domicilio;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idProveedores", fetch = FetchType.EAGER)
    private Collection<ReunionProveedores> reunionProveedoresCollection;
    @JoinColumn(name = "idUsuarios", referencedColumnName = "idUsuarios")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Usuarios idUsuarios;
    @OneToMany(mappedBy = "proveedoresidProveedores", fetch = FetchType.EAGER)
    private Collection<EventoParticipantes> eventoParticipantesCollection;

    public Proveedores() {
    }

    public Proveedores(Integer idProveedores) {
        this.idProveedores = idProveedores;
    }

    public Proveedores(Integer idProveedores, String nombre, String nombreContacto, String telefonoContacto, String telefonoEmpresa, String emailContacto, String domicilio) {
        this.idProveedores = idProveedores;
        this.nombre = nombre;
        this.nombreContacto = nombreContacto;
        this.telefonoContacto = telefonoContacto;
        this.telefonoEmpresa = telefonoEmpresa;
        this.emailContacto = emailContacto;
        this.domicilio = domicilio;
    }

    public Integer getIdProveedores() {
        return idProveedores;
    }

    public void setIdProveedores(Integer idProveedores) {
        this.idProveedores = idProveedores;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public String getTelefonoEmpresa() {
        return telefonoEmpresa;
    }

    public void setTelefonoEmpresa(String telefonoEmpresa) {
        this.telefonoEmpresa = telefonoEmpresa;
    }

    public String getEmailContacto() {
        return emailContacto;
    }

    public void setEmailContacto(String emailContacto) {
        this.emailContacto = emailContacto;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    @XmlTransient
    public Collection<ReunionProveedores> getReunionProveedoresCollection() {
        return reunionProveedoresCollection;
    }

    public void setReunionProveedoresCollection(Collection<ReunionProveedores> reunionProveedoresCollection) {
        this.reunionProveedoresCollection = reunionProveedoresCollection;
    }

    public Usuarios getIdUsuarios() {
        return idUsuarios;
    }

    public void setIdUsuarios(Usuarios idUsuarios) {
        this.idUsuarios = idUsuarios;
    }

    @XmlTransient
    public Collection<EventoParticipantes> getEventoParticipantesCollection() {
        return eventoParticipantesCollection;
    }

    public void setEventoParticipantesCollection(Collection<EventoParticipantes> eventoParticipantesCollection) {
        this.eventoParticipantesCollection = eventoParticipantesCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idProveedores != null ? idProveedores.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proveedores)) {
            return false;
        }
        Proveedores other = (Proveedores) object;
        if ((this.idProveedores == null && other.idProveedores != null) || (this.idProveedores != null && !this.idProveedores.equals(other.idProveedores))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entidad.Proveedores[ idProveedores=" + idProveedores + " ]";
    }
    
}
